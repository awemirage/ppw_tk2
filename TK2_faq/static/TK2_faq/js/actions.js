$(document).ready(
    function() {
        $(".accordionhead > a").on("click", function() {
            if ($(this).hasClass("active")) {
                $(this).removeClass("active");
                $(this).siblings(".content").slideUp();
                $(this).children().remove();
                $(this).append("<i class='fas fa-caret-down'></i>");
            }
            else{
                $(".accordionhead > a").removeClass("active");
                $(this).addClass("active");
                $(".content").slideUp();
                $(this).siblings(".content").slideDown();
                $(this).children().remove();
                $(this).append("<i class='fas fa-caret-up'></i>");
            }
        });
    }
);

function getQuestion(){
    $.ajax({
        url: '/faq/question_list',
        method: 'GET',
        type: 'json',
        success: function(result){
            var table_content= "";
            for (i=0; i<result.length; i++){
                var data = result[i].fields;
                table_content += '<tr>';
                table_content += '<th scope="row">' + data.nama + '</th>';
                table_content += '<td>' + data.pertanyaan + '</td>';
                table_content += '<tr>';
            }
            $('#questions_ajax').append(table_content);
            $('#question_button').addClass("disabled");
        }
    });
}