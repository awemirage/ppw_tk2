from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.forms.models import model_to_dict
from django.core import serializers
from . import models, forms
import json

# Create your views here.
def faqviews(request):
    if request.method == 'POST':
        form = forms.QuestionForm(request.POST)
        if form.is_valid():
            form.save()
            response = {'name' : request.POST['nama'], 'success' : True, 'form' : forms.QuestionForm()}
            return render(request, 'TK2_faq/faq-index.html', response)
    else:
        form = forms.QuestionForm()
        return render(request, 'TK2_faq/faq-index.html', {'form' : form, "success" : False})

def question_list(request):
    serialized = serializers.serialize('json', models.Question.objects.all())
    return HttpResponse(content=serialized, content_type="application/json")