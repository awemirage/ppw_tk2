from django.db import models

# Create your models here.

class Question(models.Model):
    nama = models.CharField(max_length = 40)
    email = models.EmailField()
    nomor_telepon = models.CharField(max_length = 12)
    pertanyaan = models.TextField()

    def __str__(self):
        return self.email
