from django import forms
from .models import Question

class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question

        fields = ['nama', 'email', 'nomor_telepon', 'pertanyaan']

        widgets = {
            'nama' : forms.TextInput(attrs={'class' : 'form-control', 'type' : 'text'}),
            'email' : forms.EmailInput(attrs={'class' : 'form-control', 'type' : 'email'}),
            'nomor_telepon' : forms.TextInput(attrs={'class' : 'form-control', 'type' : 'text'}),
            'pertanyaan' : forms.Textarea(attrs={'class' : 'form-control'}),
        }
