from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from django.test import TestCase, Client
from django.urls import resolve, reverse
from . import views, forms, models
from selenium import webdriver
import unittest
import time

# Create your tests here.
class TK2_FaqUnitTest(TestCase):
    #urls
    def test_faq_url_exists(self):
        response = self.client.get('/faq/')
        self.assertEqual(response.status_code, 200)

    def test_questionlist_url_exists(self):
        response = self.client.get('/faq/question_list')
        self.assertEqual(response.status_code, 200)

    #template
    def test_faq_template(self):
        response = self.client.get('/faq/')
        self.assertTemplateUsed(response, 'TK2_faq/faq-index.html')

    #views
    def test_faq_view(self):
        handler = resolve('/faq/')
        self.assertEqual(handler.func, views.faqviews)
    
    def test_question_list_view(self):
        handler = resolve('/faq/question_list')
        self.assertEqual(handler.func, views.question_list)

    #models
    def test_faq_model(self):
        qstn = models.Question.objects.create(nama='Rhendy', email='rhendyrvld@gmail.com', nomor_telepon='081212112212', pertanyaan='apaya?')
        count_row = models.Question.objects.all().count()
        self.assertEqual(count_row, 1)

    def test_faq_returns_email(self):
        qstn = models.Question.objects.create(nama='Rhendy', email='rhendyrvld@gmail.com', nomor_telepon='081212112212', pertanyaan='apaya?')
        self.assertEqual(str(qstn), 'rhendyrvld@gmail.com')

    #forms
    def test_faq_form(self):
        form = forms.QuestionForm(data = {'nama' : 'Rhendy', 
                            'email' : 'rhendyrvld@gmail.com',
                            'nomor_telepon' : '080808080',
                            'pertanyaan' : 'AAA'})
        self.assertTrue(form.is_valid())

    #post
    def test_post_sim(self):
        response = self.client.post('/faq/', {
            'nama' : 'Rhendy', 
            'email' : 'rhendyrvld@gmail.com',
            'nomor_telepon' : '080808080',
            'pertanyaan' : 'AAA'
        })
        self.assertIn("Terima kasih telah memberikan pertanyaan, Rhendy.", response.content.decode())
    