from django.urls import *
from . import views

app_name = 'TK2_faq'
urlpatterns = [
    path('', views.faqviews, name='faq-index'),
    path('question_list', views.question_list, name='question_list'),
]