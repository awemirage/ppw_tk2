from django.apps import AppConfig


class Tk2FaqConfig(AppConfig):
    name = 'TK2_faq'
