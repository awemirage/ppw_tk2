from django.db import models

# Create your models here.
class KTP(models.Model):

    name = models.CharField(max_length=16)
    email = models.EmailField(max_length=30)
    birthdate = models.DateField()
    address = models.TextField()
    handphone = models.CharField(max_length=12)
    place = models.CharField(max_length=20)

    def __str__(self):
        return self.name
