from TK2_HOME import models as home_models
from django.test import TestCase, Client
from django.urls import resolve, reverse
from . import views, forms, models
import unittest
import time

class TK2_KTPUnitTest(TestCase) :

    def test_ktp_url_exist(self) :
        response = self.client.get('/ktp/')
        self.assertEqual(response.status_code, 200)

    def test_ktp_template(self):
        response = self.client.get('/ktp/')
        self.assertTemplateUsed(response, 'TK2_KTP/ktpindex.html')

    def test_ktp_view(self):
        handler = resolve('/ktp/')
        self.assertEqual(handler.func, views.ktp)

    def test_ktp_model(self):
        KTP_test = models.KTP(name='Sendi', email='Sendi@gmail.com' , birthdate="1999-12-12", place='Medan', address='Sisingamangaraja', handphone="08111114081")
        self.assertEqual(str(KTP_test), KTP_test.name)

    def test_post_ktp(self):
        response = self.client.post('/ktp/', {
            'name': 'Insan',
            'email':'Insan@gmail.com',
            'birthdate' : '1999-12-12',
            'place' : 'Medan',
            'address': 'Panama',
            'handphone': "081234567890",
        })
        self.assertIn("You have successfully created your KTP request, Insan. Thank you for registration.", response.content.decode())

    def test_post_create_new_antrian_a(self):
        handler = resolve('/ktp/')
        count = 0
        while (count < 3):
            response = self.client.post('/ktp/', {
                'name': 'Insan',
                'email':'Insan@gmail.com',
                'birthdate' : '1999-12-12',
                'place' : 'Medan',
                'address': 'Panama',
                'handphone': "081234567890",
            })
            count += 1
        self.assertEqual(home_models.Antrian_A.objects.count(), 2)

    def test_post_create_new_antrian_b(self):
        handler = resolve('/ktp/')
        count = 0
        while (count < 4):
            response = self.client.post('/ktp/', {
                'name': 'Insan',
                'email':'Insan@gmail.com',
                'birthdate' : '1999-12-12',
                'place' : 'Medan',
                'address': 'Panama',
                'handphone': "081234567890",
            })
            count += 1
        self.assertEqual(home_models.Antrian_B.objects.count(), 2)