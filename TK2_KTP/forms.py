from django import forms

class Add_KTP(forms.Form):
    #attrs
    name_attrs = {
        'type': 'text',
        'class': 'form-control',
        'id':'name',
        'placeholder':'Timothy Wimbledon',
    }
    email_attrs = {
        'type': 'email',
        'class': 'form-control',
        'id':'email',
        'placeholder':"TimothyWimbledon@neverwinter.com",
    }
    birthdate_attrs = {
        'type': 'date',
        'class': 'form-control',
        'id':'birthdate',
    }
    handphone_attrs = {
        'type': 'number',
        'class': 'form-control',
        'id':'name',
        'placeholder':'081234567890',
    }
    address_attrs = {
        'type': 'text',
        'class': 'form-control',
        'id':'name',
        'placeholder':'Jalan Sisingamangaraja',
    }
    place = {
        'type' : 'text',
        'class': 'form-control',
        'placeholder' : 'Medan',
    }



    name = forms.CharField(label='Nama ', required=True, max_length=30, widget=forms.TextInput(attrs=name_attrs))
    email = forms.EmailField(label='Email ', required=True, widget=forms.TextInput(attrs=email_attrs))
    birthdate = forms.DateTimeField(label='Birthdate ', required=True, widget=forms.DateInput(attrs=birthdate_attrs))
    address = forms.CharField(label='Alamat ', required=True, widget=forms.Textarea(attrs=address_attrs))
    handphone = forms.IntegerField(label='Nomor Handphone ', required=True, widget=forms.TextInput(attrs=handphone_attrs))
    place = forms.CharField(label='Kota Daerah Asal', required=True, widget=forms.TextInput(attrs=place))
