from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from TK2_HOME import models as home_models
from django import forms as django_form
from django.shortcuts import render
from . import forms, models

def ktp(request):
    form = forms.Add_KTP(request.POST or None)
    response = {'form_ktp' : form, "success" : False}
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['email'] = request.POST['email']
        response['birthdate'] = request.POST['birthdate']
        response['address'] = request.POST['address']
        response['handphone'] = request.POST['handphone']
        response['place'] = request.POST['place']
        response['success'] = True
        A = home_models.Antrian_A
        B = home_models.Antrian_B
        new_antrian = ""
        count_a = int(A.objects.count())
        count_b = int(B.objects.count())
        if(count_a <= count_b):
            if(A.objects.count() == 0):
                response["no_antrian"] = 1
            else:
                response["no_antrian"] = int(A.objects.order_by('-no_antrian')[0].no_antrian) + 1
            response["jenis_antrian"] = "ktp"
            new_antrian = A(no_antrian=response["no_antrian"], name=response["name"], jenis_antrian=response["jenis_antrian"])
        elif(count_a > count_b):
            if(B.objects.count() == 0):
                response["no_antrian"] = 1
            else:
                response["no_antrian"] = int(B.objects.order_by('-no_antrian')[0].no_antrian) + 1
            response["jenis_antrian"] = "ktp"
            new_antrian = B(no_antrian=response["no_antrian"], name=response["name"], jenis_antrian=response["jenis_antrian"])
        new_antrian.save()
        new_ktp = models.KTP(name=response['name'], email=response['email'], birthdate=response['birthdate'], address=response['address'], handphone=response['handphone'], place=response['place'])
        new_ktp.save()
    return render(request, 'TK2_KTP/ktpindex.html', response)
