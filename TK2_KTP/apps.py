from django.apps import AppConfig


class Tk2KtpConfig(AppConfig):
    name = 'TK2_KTP'
