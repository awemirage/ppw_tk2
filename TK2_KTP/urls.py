from django.urls import path
from . import views

app_name='TK2_KTP'
urlpatterns = [
    path('', views.ktp, name='ktp'),
]
