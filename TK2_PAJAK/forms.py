from django import forms

class Add_Pajak(forms.Form):
    #attrs
    name_attrs = {
        'type': 'text',
        'class': 'form-control',
        'id':'name',
        'placeholder':'Timothy Wimbledon',
    }
    ktp_attrs = {
        'type': 'number',
        'class': 'form-control',
        'id':'ktp',
        'placeholder':'3175033108990000',
    }
    handphone_attrs = {
        'type': 'number',
        'class': 'form-control',
        'id':'name',
        'placeholder':'085779766000',
    }
    address_attrs = {
        'type': 'text',
        'class': 'form-control',
        'id':'name',
        'placeholder':'Kyrkstadsvägen 15, 931 33 Skellefteå, Sweden',
    }
    vehicle_type_attrs = {
        'class': 'form-control',
    }
    
    #choice
    Vehicle_List = [(1, "Motor"), (2, "Mobil"), (3, "Truk"), (4, "Bus"), (5, "Pickup"), (6, "Box"), (7, "Tracktor"), (8, "Kendaraan Berat")]

    name = forms.CharField(label='Nama ', required=True, max_length=30, widget=forms.TextInput(attrs=name_attrs))
    ktp = forms.CharField(label='Nomor KTP ', widget=forms.TextInput(attrs=ktp_attrs))
    address = forms.CharField(label='Alamat ', required=True, widget=forms.Textarea(attrs=address_attrs))
    handphone = forms.IntegerField(label='Nomor Handphone ', required=True, widget=forms.TextInput(attrs=handphone_attrs))
    vehicle_type = forms.ChoiceField(label='Tipe Kendaraan ', choices=Vehicle_List, required=True, widget=forms.Select(attrs=vehicle_type_attrs))