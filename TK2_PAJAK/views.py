from django.http.response import JsonResponse
from django.http import HttpResponseRedirect
from TK2_HOME import models as home_models
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse
from .models import *
from .forms import *
# Create your views here.
def pajakviews(request):
    form = Add_Pajak(request.POST or None)
    response = {'form_pajak' : form, "success" : False}
    if(request.method == 'POST' and form.is_valid()):
        Vehicle_List = ["Motor", "Mobil", "Truk", "Bus", "Pickup", "Box", "Tracktor", "Kendaraan Berat"]
        response['name'] = request.POST['name']
        response['ktp'] = request.POST['ktp']
        response['address'] = request.POST['address']
        response['handphone'] = request.POST['handphone']
        response['vehicle_type'] = request.POST['vehicle_type']
        response['success'] = True
        A = home_models.Antrian_A
        B = home_models.Antrian_B
        new_antrian = ""
        count_a = int(A.objects.count())
        count_b = int(B.objects.count())
        if(count_a <= count_b):
            if(A.objects.count() == 0):
                response["no_antrian"] = 1
            else:
                response["no_antrian"] = int(A.objects.order_by('-no_antrian')[0].no_antrian) + 1
            response["jenis_antrian"] = "pajak"
            new_antrian = A(no_antrian=response["no_antrian"], name=response["name"], jenis_antrian=response["jenis_antrian"])
        elif(count_a > count_b):
            if(B.objects.count() == 0):
                response["no_antrian"] = 1
            else:
                response["no_antrian"] = int(B.objects.order_by('-no_antrian')[0].no_antrian) + 1
            response["jenis_antrian"] = "pajak"
            new_antrian = B(no_antrian=response["no_antrian"], name=response["name"], jenis_antrian=response["jenis_antrian"])
        new_antrian.save()
        new_pajak = Pajak(name=response['name'], ktp=response['ktp'], address=response['address'], handphone=response['handphone'], vehicle_type=response['vehicle_type'])
        new_pajak.save()
        response['sim_version'] = Vehicle_List[int(response['vehicle_type']) - 1]
    return render(request, 'TK2_PAJAK/pajak-index.html', response)

def list_pajak(request):
	pajak_list = Pajak.objects.all().values()
	return JsonResponse(list(pajak_list), safe=False)