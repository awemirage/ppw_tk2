from django.apps import AppConfig


class Tk2PajakConfig(AppConfig):
    name = 'TK2_PAJAK'
