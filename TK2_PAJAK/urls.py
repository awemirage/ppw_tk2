from django.urls import *
from . import views
from .views import pajakviews

app_name = 'TK2_PAJAK'
urlpatterns = [
    path('', views.pajakviews, name='pajak-index'),
    path('list_pajak/', views.list_pajak, name='list_pajak'),
]