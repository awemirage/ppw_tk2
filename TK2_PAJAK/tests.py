from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from TK2_HOME import models as home_models
from django.test import TestCase, Client
from django.urls import resolve, reverse
from . import views, forms, models
from selenium import webdriver
import unittest
import time

# Create your tests here.
class TK2_PajakUnitTest(TestCase):
    #urls
    def test_pajak_url_exists(self):
        response = self.client.get('/pajak/')
        self.assertEqual(response.status_code, 200)

    def test_list_pajak_url_exists(self):
        response = self.client.get('/pajak/list_pajak/')
        self.assertEqual(response.status_code, 200)

    #template
    def test_pajak_template(self):
        response = self.client.get('/pajak/')
        self.assertTemplateUsed(response, 'TK2_PAJAK/pajak-index.html')

    #views
    def test_pajak_view(self):
        handler = resolve('/pajak/')
        self.assertEqual(handler.func, views.pajakviews)

    def test_list_pajak_view(self):
        handler = resolve('/pajak/list_pajak/')
        self.assertEqual(handler.func, views.list_pajak)

    #models
    def test_pajak_models(self):
        PAJAK_test = models.Pajak(name='Randy', ktp='3175032403990013', address='Porapora', handphone='085656565656', vehicle_type='Mobil')
        self.assertEqual(str(PAJAK_test), PAJAK_test.name)

    #froms
    def test_pajak_forms(self):
        form_data = {
            'name': 'Randy',
            'ktp': '3175032403990013',
            'address': 'Porapora',
            'handphone': '085656565656',
            'vehicle_type': 1,
        }
        pajak_form = forms.Add_Pajak(data=form_data)
        self.assertTrue(pajak_form.is_valid())

    #post
    def test_post_pajak(self):
        password = 'dummy12345'
        dummy_user = User.objects.create_user('dummy', 'dummy@test.com', password)
        dummy_user.is_staff = True
        self.client.login(username=dummy_user.username, password=password)
        response = self.client.post('/pajak/', {
            'name': 'Randy',
            'ktp': '3175032403990013',
            'address': 'Porapora',
            'handphone': '085656565656',
            'vehicle_type': 1,
        })
        self.assertIn("Terima kasih telah membayar pajak Randy.", response.content.decode())

    def test_post_create_new_antrian_a(self):
        count = 0
        while (count < 3):
            response = self.client.post('/pajak/', {
                'name': 'Randy',
                'ktp': '3175032403990013',
                'address': 'Porapora',
                'handphone': '085656565656',
                'vehicle_type': 1,
            })
            count += 1
        self.assertEqual(home_models.Antrian_A.objects.count(), 2)

    def test_post_create_new_antrian_b(self):
        count = 0
        while (count < 4):
            response = self.client.post('/pajak/', {
            'name': 'Randy',
            'ktp': '3175032403990013',
            'address': 'Porapora',
            'handphone': '085656565656',
            'vehicle_type': 1,
            })
            count += 1
        self.assertEqual(home_models.Antrian_B.objects.count(), 2)