from django.db import models

# Create your models here.
class Pajak(models.Model):
    Vehicle_List = [(1, "Motor"), (2, "Mobil"), (3, "Truk"), (4, "Bus"), (5, "Pickup"), (6, "Box"), (7, "Tracktor"), (8, "Kendaraan Berat")]

    name = models.CharField(max_length=30)
    ktp = models.CharField(max_length=16)
    address = models.TextField()
    handphone = models.CharField(max_length=12)
    vehicle_type = models.CharField(choices=Vehicle_List, max_length=1)

    def __str__(self):
        return self.name