from django import forms

class Add_Sim(forms.Form):
    #attrs
    name_attrs = {
        'type': 'text',
        'class': 'form-control',
        'id':'name',
        'placeholder':'Timothy Wimbledon',
    }
    ktp_attrs = {
        'type': 'number',
        'class': 'form-control',
        'id':'ktp',
        'placeholder':'3175033108990000',
    }
    handphone_attrs = {
        'type': 'number',
        'class': 'form-control',
        'id':'name',
        'placeholder':'081234567890',
    }
    address_attrs = {
        'type': 'text',
        'class': 'form-control',
        'id':'name',
        'placeholder':'Kyrkstadsvägen 15, 931 33 Skellefteå, Sweden',
    }
    sim_type_attrs = {
        'class': 'form-control',
    }
    
    #choice
    Sim_List = [(1, "SIM A"), (2, "SIM B1"), (3, "SIM B2"), (4, "SIM C"), (5, "SIM C1"), (6, "SIM C2"), (7, "SIM D"), (8, "SIM D1"), (9, "SIM Internasional")]

    name = forms.CharField(label='Nama ', required=True, max_length=30, widget=forms.TextInput(attrs=name_attrs))
    ktp = forms.CharField(label='Nomor KTP ', widget=forms.TextInput(attrs=ktp_attrs))
    address = forms.CharField(label='Alamat ', required=True, widget=forms.Textarea(attrs=address_attrs))
    handphone = forms.IntegerField(label='Nomor Handphone ', required=True, widget=forms.TextInput(attrs=handphone_attrs))
    sim_type = forms.ChoiceField(label='Tipe Sim ', choices=Sim_List, required=True, widget=forms.Select(attrs=sim_type_attrs))