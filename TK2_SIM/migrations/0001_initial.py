# Generated by Django 2.2.6 on 2019-12-06 15:39

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Sim',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
                ('ktp', models.CharField(max_length=16)),
                ('address', models.TextField()),
                ('handphone', models.CharField(max_length=12)),
                ('sim_type', models.CharField(choices=[(1, 'SIM A'), (2, 'SIM B1'), (3, 'SIM B2'), (4, 'SIM C'), (5, 'SIM C1'), (6, 'SIM C2'), (7, 'SIM D'), (8, 'SIM D1'), (9, 'SIM Internasional')], max_length=1)),
            ],
        ),
    ]
