from django.db import models

# Create your models here.
class SIM(models.Model):
    Sim_List = [(1, "SIM A"), (2, "SIM B1"), (3, "SIM B2"), (4, "SIM C"), (5, "SIM C1"), (6, "SIM C2"), (7, "SIM D"), (8, "SIM D1"), (9, "SIM Internasional")]

    name = models.CharField(max_length=30)
    ktp = models.CharField(max_length=16)
    address = models.TextField()
    handphone = models.CharField(max_length=12)
    sim_type = models.CharField(choices=Sim_List, max_length=1)

    def __str__(self):
        return self.name