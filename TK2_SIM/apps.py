from django.apps import AppConfig


class Tk2SimConfig(AppConfig):
    name = 'TK2_SIM'
