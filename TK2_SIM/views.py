from django.http.response import JsonResponse
from TK2_HOME import models as home_models
from django.shortcuts import render
from . import forms, models

# Create your views here.
def sim(request):
    form = forms.Add_Sim(request.POST or None)
    response = {'form_sim' : form, "success" : False}
    if(request.method == 'POST' and form.is_valid()):
        Sim_List = ["SIM A", "SIM B1", "SIM B2", "SIM C", "SIM C1", "SIM C2", "SIM D", "SIM D1", "SIM Internasional"]
        response['name'] = request.POST['name']
        response['ktp'] = request.POST['ktp']
        response['address'] = request.POST['address']
        response['handphone'] = request.POST['handphone']
        response['sim_type'] = request.POST['sim_type']
        response['success'] = True
        A = home_models.Antrian_A
        B = home_models.Antrian_B
        new_antrian = ""
        count_a = int(A.objects.count())
        count_b = int(B.objects.count())
        if(count_a <= count_b):
            if(A.objects.count() == 0):
                response["no_antrian"] = 1
            else:
                response["no_antrian"] = int(A.objects.order_by('-no_antrian')[0].no_antrian) + 1
            response["jenis_antrian"] = "sim"
            new_antrian = A(no_antrian=response["no_antrian"], name=response["name"], jenis_antrian=response["jenis_antrian"])
        elif(count_a > count_b):
            if(B.objects.count() == 0):
                response["no_antrian"] = 1
            else:
                response["no_antrian"] = int(B.objects.order_by('-no_antrian')[0].no_antrian) + 1
            response["jenis_antrian"] = "sim"
            new_antrian = B(no_antrian=response["no_antrian"], name=response["name"], jenis_antrian=response["jenis_antrian"])
        new_antrian.save()
        new_sim = models.SIM(name=response['name'], ktp=response['ktp'], address=response['address'], handphone=response['handphone'], sim_type=response['sim_type'])
        new_sim.save()
        response['sim_version'] = Sim_List[int(response['sim_type']) - 1]
    return render(request, 'TK2_SIM/sim.html', response)

def list_sim(request):
	sim_list = models.SIM.objects.all().values()
	return JsonResponse(list(sim_list), safe=False)