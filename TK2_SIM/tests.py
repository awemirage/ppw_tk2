from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from TK2_HOME import models as home_models
from django.test import TestCase, Client
from django.urls import resolve, reverse
from . import views, forms, models
from selenium import webdriver
import unittest
import time

# Create your tests here.
class TK2_SIMUnitTest(TestCase):
    #urls
    def test_sim_url_exists(self):
        response = self.client.get('/sim/')
        self.assertEqual(response.status_code, 200)

    def test_list_sim_url_exists(self):
        response = self.client.get('/sim/list_sim/')
        self.assertEqual(response.status_code, 200)

    #template
    def test_sim_template(self):
        response = self.client.get('/sim/')
        self.assertTemplateUsed(response, 'TK2_SIM/sim.html')

    #views
    def test_sim_view(self):
        handler = resolve('/sim/')
        self.assertEqual(handler.func, views.sim)

    def test_list_sim_view(self):
        handler = resolve('/sim/list_sim/')
        self.assertEqual(handler.func, views.list_sim)

    def test_post_create_new_antrian_a(self):
        count = 0
        while (count < 3):
            response = self.client.post('/sim/', {
                'name': 'Insan',
                'ktp': '3175033108991234',
                'address': 'Panama', 
                'handphone': "081234567890", 
                'sim_type': 1, 
            })
            count += 1
        self.assertEqual(home_models.Antrian_A.objects.count(), 2)

    def test_post_create_new_antrian_b(self):
        count = 0
        while (count < 4):
            response = self.client.post('/sim/', {
                'name': 'Insan',
                'ktp': '3175033108991234',
                'address': 'Panama', 
                'handphone': "081234567890", 
                'sim_type': 1, 
            })
            count += 1
        self.assertEqual(home_models.Antrian_B.objects.count(), 2)

    #models
    def test_sim_model(self):
        SIM_test = models.SIM(name='Insan', ktp='3175033108991234', address='Panama', handphone="081234567890", sim_type="SIM A")
        self.assertEqual(str(SIM_test), SIM_test.name)

    #forms
    def test_sim_form(self):
        form_data = {
            'name': 'Insan',
            'ktp': '3175033108991234',
            'address': 'Panama', 
            'handphone': "081234567890", 
            'sim_type': 1, 
        }
        sim_form = forms.Add_Sim(data=form_data)
        self.assertTrue(sim_form.is_valid())

    #post
    def test_post_sim(self):
        password = 'dummy12345'
        dummy_user = User.objects.create_user('dummy', 'dummy@test.com', password)
        dummy_user.is_staff = True
        self.client.login(username=dummy_user.username, password=password)
        response = self.client.post('/sim/', {
            'name': 'Insan',
            'ktp': '3175033108991234',
            'address': 'Panama', 
            'handphone': "081234567890", 
            'sim_type': 1, 
        })
        self.assertIn("You have created SIM A, Insan. Thank you for registration.", response.content.decode())