from django.urls import path
from . import views

app_name='TK2_SIM'
urlpatterns = [
    path('', views.sim, name='sim'),
    path('list_sim/', views.list_sim, name='list_sim'),
]