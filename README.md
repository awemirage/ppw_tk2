# Tarung2017

Tugas Kelompok Kelompok 1

Anggota Kelompok    
1.  Insanul Fahmi                   - 1706979291
2.  Muhammad Sendi Siradj           - 1706043771
3.  Randy Hidayah Putra Desnantara  - 1706044130
4.  Rhendy Rivaldo                  - 1706039982



# Sipemerintah

Sipemerintah adalah website yang memiliki tujuan untuk memudahkan masyarakat dalam melakukan kegiatan administrasi dan pembayaran pajak. Sipemerintah membantu masyarakat dengan cara memberikan layanan untuk melakukan kegiatan tersebut secara online. Layanan tersebut berisi tentang otomasi tahapan yang dapat dilakukan di mana saja asalkan terhubung dengan internet. Otomasi ini yaitu pengumpulan berkas yang dilakukan secara online sehingga mengurangi waktu yang dibutuhkan masyarakat dalam melakukan proses administrasi dan memudahkan masyarakat dalam mengumpulkan dokumen yang terkait untuk proses administrasi yang diinginkan. Website ini memiliki beberapa fitur antara lain pendaftaran penduduk, pembuatan dan perpanjangan SIM, pembayaran pajak kendaraan, serta FAQ.

Aplikasi yang berada di Website Sipemerintah antara lain

1.  Pendaftaran Penduduk

    Aplikasi Pendaftaran penduduk merupakan aplikasi yang membantu masyarakat dalam mendaftarkan dirinya sebagai penduduk Republik Indonesia. Fitur yang tersedia dari aplikasi ini antara lain pembuatan KTP.

2.  Pembuatan dan Perpanjangan SIM Kendaraan Bermotor

    Aplikasi Pembuatan dan Perpanjangan SIM Kendaraan Bermotor merupakan aplikasi yang membantu masyarakat dalam proses pembuatan dan perpanjangan SIM Kendaraan Bermotor yang diinginkan. Fitur yang tersedia dari aplikasi ini antara lain pembuatan SIM dan perpanjangan SIM.

3.  Pembayaran Pajak Kendaraan

    Aplikasi Pembayaran Pajak Kendaraan merupakan aplikasi yang membantu masyarakat dalam proses pembayaran pajak kendaraan yang dimiliki oleh penduduk tersebut. Fitur yang tersedia dari aplikasi ini antara lain pembayaran pajak kendaraan. 

4.  FAQ

    Aplikasi Frequently Asked Question merupakan aplikasi yang membantu masyarakat untuk mengetahui lebih mengenai fungsi dari website Sipemerintah. Fitur yang tersedia dari aplikasi ini antara lain Contact Me.

5.  Admin

    Aplikasi Admin untuk memeriksa request pembuatan KTP, Pajak, dan SIM

# Pembagian Tugas

*   Home Screen     : Semua
*   KTP             : Muhammad Sendi Siradj
*   Sim             : Randy Hidayah Putra Desnantara
*   Pajak           : Insanul Fahmi
*   FAQ             : Rhendy Rivaldo
*   Login           : Semua
*   Admin           : Semua  
  

* * *

# Link Wireframe

* Landing Page https://wireframe.cc/eDujdL
* About https://wireframe.cc/2qc0ru
* Pembuatan KTP https://wireframe.cc/kXxSdD
* Pembuatan SIM https://wireframe.cc/Bsr4Lf
* Pembayaran Pajak Kendaraan https://wireframe.cc/fTUgqT
* FAQ https://wireframe.cc/qN78TT


* * *

# Link Mockup

https://www.figma.com/file/kp5j0HIpL2WaCxpN5c9XlE/Untitled?node-id=0%3A1

* * *

# Link herokuapp 

https://ppwtk2tarung2017.herokuapp.com/

* * *

# Coverage Status

[![coverage report](https://gitlab.com/awemirage/ppw_tk2/badges/master/coverage.svg)](https://gitlab.com/awemirage/ppw_tk2/commits/master)

* * *

# Pipelines Status

[![pipeline status](https://gitlab.com/awemirage/ppw_tk2/badges/master/pipeline.svg)](https://gitlab.com/awemirage/ppw_tk2/commits/master)

* * *