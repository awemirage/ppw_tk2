$(document).ready(function(){
    if (window.location.pathname == "/ktp/") {
        $("#register_href").addClass("disabled"); 
        $("#register_href").addClass('nav_used');
    } else if (window.location.pathname == "/login/") {
        $("#login_href").addClass("disabled"); 
        $("#login_href").addClass('nav_used');
    } else if (window.location.pathname == "/sim/") {
        $("#sim_href").addClass("disabled"); 
        $("#sim_href").addClass('nav_used');
    } else if (window.location.pathname == "/pajak/") {
        $("#pajak_href").addClass("disabled"); 
        $("#pajak_href").addClass('nav_used');
    } else if (window.location.pathname == "/faq/") {
        $("#faq_href").addClass("disabled"); 
        $("#faq_href").addClass('nav_used');
    } if (window.location.pathname == "/" || window.location.pathname == "/login/") {
        $(".nav-item").addClass("transparent-bg-gray"); 
        $(".nav-item").addClass("rounded");
    } 
});

function getAntrianA(){
    $.ajax({
        url: '/admin_page/antrian_list_a/',
        method: 'GET',
        type: 'json',
        success: function(result){
            var table_content= "";
            for (i=0; i<result.length; i++){
                var data = result[i];
                table_content += '<tr>';
                table_content += '<th scope="row">' + data.no_antrian + '</th>';
                table_content += '<td>' + data.name + '</td>';
                table_content += '<td>' + data.jenis_antrian + '</td>';
                table_content += '<tr>';
            }
            $('#antrian_a_ajax').append(table_content);
            $('#antrian_a_button').addClass("disabled");
        }
    });
}

function getAntrianB(){
    $.ajax({
        url: '/admin_page/antrian_list_b/',
        method: 'GET',
        type: 'json',
        success: function(result){
            var table_content= "";
            for (i=0; i<result.length; i++){
                var data = result[i];
                table_content += '<tr>';
                table_content += '<th scope="row">' + data.no_antrian + '</th>';
                table_content += '<td>' + data.name + '</td>';
                table_content += '<td>' + data.jenis_antrian + '</td>';
                table_content += '<tr>';
            }
            $('#antrian_b_ajax').append(table_content);
            $('#antrian_b_button').addClass("disabled");
        }
    });
}