from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from django.test import TestCase, Client
from django.urls import resolve, reverse
from . import views, forms, models
from selenium import webdriver
import unittest
import time

# Create your tests here.
class TK2_HOMEUnitTest(TestCase):
    #urls
    def test_index_url_exists(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)
        
    def test_login_url_exists(self):
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 200)
    
    def test_register_url_exists(self):
        response = self.client.get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_register_logout_exists(self):
        response = self.client.get('/logout/')
        self.assertEqual(response.status_code, 302)

    def test_admin_page_url_not_staff_exists(self):
        response = self.client.get('/admin_page/')
        self.assertEqual(response.status_code, 302)

    def test_antrian_list_a_url_exists(self):
        response = self.client.get('/admin_page/antrian_list_a/')
        self.assertEqual(response.status_code, 200)

    def test_antrian_list_b_url_exists(self):
        response = self.client.get('/admin_page/antrian_list_b/')
        self.assertEqual(response.status_code, 200)

    #template
    def test_index_template(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'TK2_HOME/index.html')

    def test_login_template(self):
        response = self.client.get('/login/')
        self.assertTemplateUsed(response, 'TK2_HOME/login.html')
    
    def test_register_template(self):
        response = self.client.get('/register/')
        self.assertTemplateUsed(response, 'TK2_HOME/register.html')

    #views
    def test_index_view(self):
        handler = resolve('/')
        self.assertEqual(handler.func, views.index)

    def test_admin_page_view(self):
        handler = resolve('/admin_page/')
        self.assertEqual(handler.func, views.admin_page)

    def test_antrian_list_a_view(self):
        handler = resolve('/admin_page/antrian_list_a/')
        self.assertEqual(handler.func, views.antrian_list_a)

    def test_antrian_list_b_view(self):
        handler = resolve('/admin_page/antrian_list_b/')
        self.assertEqual(handler.func, views.antrian_list_b)

    #models
    def test_antrian_a_models(self):
        antrian_test = models.Antrian_A(no_antrian=1, name='Insan', jenis_antrian='ktp')
        self.assertEqual(str(antrian_test), antrian_test.name)

    def test_antrian_b_models(self):
        antrian_test = models.Antrian_B(no_antrian=1, name='Insan', jenis_antrian='ktp')
        self.assertEqual(str(antrian_test), antrian_test.name)

    def test_account_models(self):
        account_test = models.Account(name='Randy', ktp=3175033108990000, email='randy@gmail.com', type='Admin')
        self.assertEqual(str(account_test), account_test.name)





