from django.apps import AppConfig


class Tk2HomeConfig(AppConfig):
    name = 'TK2_HOME'
