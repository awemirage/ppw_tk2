from django.db import models

# Create your models here.
class Antrian_A(models.Model):
    no_antrian = models.CharField(max_length=5)
    name = models.CharField(max_length=16)
    jenis_antrian = models.CharField(max_length=5)
   
    def __str__(self):
        return self.name

class Antrian_B(models.Model):
    no_antrian = models.CharField(max_length=5)
    name = models.CharField(max_length=16)
    jenis_antrian = models.CharField(max_length=5)
   
    def __str__(self):
        return self.name

class Account(models.Model):     
    name = models.CharField(max_length=16)
    ktp = models.IntegerField()
    email = models.EmailField(max_length=30)
    type = models.CharField(max_length=8)
   
    def __str__(self):
        return self.name
