from django.urls import path, include
from django.contrib.auth import views as auth_view
from django.contrib.auth.forms import AuthenticationForm
from . import views
from . import forms

app_name='TK2_HOME'
urlpatterns = [
    path('', views.index, name='index'),
    path('register/', views.register, name="register"),
    path('login/', auth_view.LoginView.as_view(template_name='TK2_HOME/login.html', form_class=forms.LoginForm), name='login'),
    path('logout/', auth_view.LogoutView.as_view(), name='logout'),
    path('admin_page/', views.admin_page, name='admin_page'),
    path('admin_page/antrian_list_a/', views.antrian_list_a, name='antrian_list_a'),
    path('admin_page/antrian_list_b/', views.antrian_list_b, name='antrian_list_b'),
]