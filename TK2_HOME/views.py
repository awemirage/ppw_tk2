from django.shortcuts import render, HttpResponseRedirect
from django.http.response import JsonResponse
from django.urls import reverse
from . import models
from TK2_KTP import models as models_ktp
from TK2_SIM import models as models_sim
from TK2_PAJAK import models as models_pajak

# Create your views here.
def index(request):
    response = {}
    return render(request, 'TK2_HOME/index.html', response)

def register(request):
    response = {}
    return render(request, 'TK2_HOME/register.html', response)

def admin_page(request):
    if(request.user.is_staff):
        response = {}
        return render(request, 'TK2_HOME/admin_page.html', response)
    else:
        return HttpResponseRedirect(reverse("TK2_HOME:index"))

def antrian_list_a(request):
	views_antrian_A = models.Antrian_A.objects.all().values()
	return JsonResponse(list(views_antrian_A), safe=False)

def antrian_list_b(request):
	views_antrian_B = models.Antrian_B.objects.all().values()
	return JsonResponse(list(views_antrian_B), safe=False)